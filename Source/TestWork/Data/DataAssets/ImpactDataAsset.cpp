// Fill out your copyright notice in the Description page of Project Settings.
#include "Data/DataAssets/ImpactDataAsset.h"

USoundBase* UImpactDataAsset::GetRandomSound()
{
	if (ImpactSounds.Num() != 0)
	{
		return ImpactSounds[FMath::RandHelper(ImpactSounds.Num())];
	}

	return nullptr;
}