// Fill out your copyright notice in the Description page of Project Settings.

#include "TestPlayerController.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Components/HealthComponent.h"
#include "GameFramework/GameModeBase.h"
#include "GameFramework/PlayerStart.h"
#include "Kismet/GameplayStatics.h"

void ATestPlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if(auto HealthComponent = Cast<UHealthComponent>(InPawn->GetComponentByClass(UHealthComponent::StaticClass())))
	{
		HealthComponent->ResetHealth();
		HealthComponent->CharDead.AddDynamic(this, &ATestPlayerController::CharacterDead);
	}
}

void ATestPlayerController::ToggleMenu()
{
	
}

void ATestPlayerController::CharacterDead()
{
	//Deleted character from world
	GetPawn()->SetLifeSpan(5.f);

	//Create deadscreen for respawn
	DeadScreenWidget = CreateWidget(this, DeadScreenWidgetClass);
	DeadScreenWidget->AddToViewport();
	UWidgetBlueprintLibrary::SetInputMode_UIOnlyEx(this, DeadScreenWidget);
	SetShowMouseCursor(true);
}

void ATestPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	// Bind toggle inventory event
	//InputComponent->BindAction("Inventory", IE_Pressed, this, &ATestPlayerController::ToggleInventory);
}

void ATestPlayerController::RespawnCharacter()
{
	if(DeadScreenWidget)
	{
		DeadScreenWidget->RemoveFromParent();
		DeadScreenWidget = nullptr;
		UWidgetBlueprintLibrary::SetInputMode_GameOnly(this);
		SetShowMouseCursor(false);
	}
	
	auto CurrentGameMode = UGameplayStatics::GetGameMode(this);
	if(auto NewPlayerStart = Cast<APlayerStart>(UGameplayStatics::GetActorOfClass(this, APlayerStart::StaticClass())))
	{
		if(auto NewPawn = GetWorld()->SpawnActorDeferred<APawn>(CurrentGameMode->DefaultPawnClass, NewPlayerStart->GetActorTransform()))
		{
			NewPawn->FinishSpawning(NewPlayerStart->GetActorTransform());
			OnRespawnCharacter.Broadcast(NewPawn);
			Possess(NewPawn);
		}
	}
}

void ATestPlayerController::BeginPlay()
{
	Super::BeginPlay();

	MainScreenWidget = CreateWidget(this, MainScreenWidgetClass);
	MainScreenWidget->AddToViewport(100);
	
}
