// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCharDead);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FChangeHealth, float, CurrentHealth, FString, NameComponent);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TESTWORK_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	/** Maximum object health */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "StatusComponent|Health")
	float MaxHealth = 100.f;

	/** Current object health */
	UPROPERTY(BlueprintReadWrite, Category = "StatusComponent|Health")
	float Health;

	/** Is the object currently dead? */
	UPROPERTY(BlueprintReadOnly)
	bool bIsDead;

	/** Called when an object dies */
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FCharDead CharDead;

	/** Called at any change in the health of the object */
	UPROPERTY(BlueprintAssignable, BlueprintCallable)
	FChangeHealth OnChangeHealth;
	
	/** Restores the health of the object */
	UFUNCTION(BlueprintCallable)
	void ResetHealth();

	UFUNCTION(BlueprintPure)
	void GetHealth(float& Max, float& Current) const;
	
	void BeginPlay() override;

protected:
	
	UFUNCTION()
	void OnPlayerAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
						   class AController* InstigatedBy, AActor* DamageCauser);
};
