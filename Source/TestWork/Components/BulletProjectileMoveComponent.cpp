// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/BulletProjectileMoveComponent.h"

void UBulletProjectileMoveComponent::RestartSimulation()
{
	bStoppedSimulation = false;
	
	Velocity = FVector(1.f, 0.f, 0.f);

	if (bSnapToPlaneAtStart)
	{
		SnapUpdatedComponentToPlane();
	}

	if (Velocity.SizeSquared() > 0.f)
	{
		// InitialSpeed > 0 overrides initial velocity magnitude.
		if (InitialSpeed > 0.f)
		{
			Velocity = Velocity.GetSafeNormal() * InitialSpeed;
		}

		if (bInitialVelocityInLocalSpace)
		{
			SetVelocityInLocalSpace(Velocity);
		}

		if (bRotationFollowsVelocity)
		{
			if (UpdatedComponent)
			{
				FRotator DesiredRotation = Velocity.Rotation();
				if (bRotationRemainsVertical)
				{
					DesiredRotation.Pitch = 0.0f;
					DesiredRotation.Yaw = FRotator::NormalizeAxis(DesiredRotation.Yaw);
					DesiredRotation.Roll = 0.0f;
				}

				UpdatedComponent->SetWorldRotation(DesiredRotation);
			}
		}

		UpdateComponentVelocity();

		if (UpdatedPrimitive && UpdatedPrimitive->IsSimulatingPhysics())
		{
			UpdatedPrimitive->SetPhysicsLinearVelocity(Velocity);
		}
	}
}

void UBulletProjectileMoveComponent::StopSimulating(const FHitResult& HitResult)
{
	bStoppedSimulation = true;
	Velocity = FVector::ZeroVector;
	UpdateComponentVelocity();
	OnProjectileStop.Broadcast(HitResult);
}

void UBulletProjectileMoveComponent::SetUpdatedComponent(USceneComponent* NewUpdatedComponent)
{
	if (!UpdatedComponent)
	{
		Super::SetUpdatedComponent(NewUpdatedComponent);
	}
}

void UBulletProjectileMoveComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	if(!bStoppedSimulation)
	{
		Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	}
}
