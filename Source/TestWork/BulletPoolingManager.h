// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Objects/Weapons/Bullets/BaseBulletProjectile.h"
#include "Subsystems/WorldSubsystem.h"
#include "BulletPoolingManager.generated.h"

/**
 * 
 */
UCLASS()
class TESTWORK_API UBulletPoolingManager : public UWorldSubsystem
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Surface Footstep System", meta = (WorldContext = "WorldContextObject", CallableWithoutWorldContext))
	static void DestroyBulletPool(const UObject* WorldContextObject);
	
	void SafeSpawnBulletActor(TSubclassOf<ABaseBulletProjectile> Class, FTransform Trans);
	void DestroyBulletActors();
	ABaseBulletProjectile* GetBulletActorByClass(TSubclassOf<ABaseBulletProjectile> BulletClass);
	
private:
	UPROPERTY()
	TArray<ABaseBulletProjectile*> BulletActors;
};
