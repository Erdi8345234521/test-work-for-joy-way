// Fill out your copyright notice in the Description page of Project Settings.


#include "Objects/TargetEnemy.h"

#include "Components/TextRenderComponent.h"



// Sets default values
ATargetEnemy::ATargetEnemy()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create root scene component..
	MyRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("MyRootComponent"));
	SetRootComponent(MyRootComponent);

	//Create mesh component...
	MeshEnemy = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshEnemy"));
	MeshEnemy->SetupAttachment(GetRootComponent());

	//Create health component...
	HealthComponent = CreateDefaultSubobject<UHealthComponent>(TEXT("HelthComponent"));
	HealthComponent->CharDead.AddDynamic(this, &ATargetEnemy::OnDead);

	//Create text state component
	TextState = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextState"));
	TextState->SetupAttachment(GetRootComponent());
	TextState->SetText(NSLOCTEXT("Enemy", "Enemy.State", "Alife"));
	TextState->HorizontalAlignment = EHTA_Center;
}

void ATargetEnemy::OnDead()
{
	TextState->SetText(NSLOCTEXT("Enemy", "Enemy.State", "Dead"));
	GetWorldTimerManager().SetTimer(RespawnTimer, this, &ATargetEnemy::Respawn, RespawnTime);
}

void ATargetEnemy::Respawn()
{
	TextState->SetText(NSLOCTEXT("Enemy", "Enemy.State", "Alife"));
	HealthComponent->ResetHealth();
}
