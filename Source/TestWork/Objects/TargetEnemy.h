// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/HealthComponent.h"
#include "GameFramework/Actor.h"
#include "TargetEnemy.generated.h"

UCLASS()
class TESTWORK_API ATargetEnemy : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATargetEnemy();

	UPROPERTY(VisibleAnywhere)
	USceneComponent* MyRootComponent;
	
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshEnemy;

	UPROPERTY(VisibleAnywhere)
	UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere)
	class UTextRenderComponent* TextState;

	UPROPERTY(EditAnywhere)
	float RespawnTime = 5.f;
	
	UFUNCTION()
	void OnDead();

	FTimerHandle RespawnTimer;
	void Respawn();
};
