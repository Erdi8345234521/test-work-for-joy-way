// Fill out your copyright notice in the Description page of Project Settings.

#include "Objects/Weapons/Bullets/BaseBulletProjectile.h"

#include "Components/AudioComponent.h"
#include "Components/BulletProjectileMoveComponent.h"
#include "Components/DecalComponent.h"
#include "Components/SphereComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Objects/Weapons/BaseWeaponItem.h"
#include "Particles/ParticleSystemComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
ABaseBulletProjectile::ABaseBulletProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create collision collider...
	Collider = CreateDefaultSubobject<USphereComponent>(TEXT("CPP_Colider"));
	Collider->SetCollisionProfileName("Bullet");
	SetRootComponent(Collider);

	//Create audio component...
	AudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	AudioComponent->bAutoActivate = false;
	AudioComponent->SetupAttachment(GetRootComponent());

	//Create bullet mesh component...
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BulletMesh"));
	BulletMesh->SetupAttachment(GetRootComponent());
	
	//Create projectileMovement component...
	ProjectileMovement = CreateDefaultSubobject<UBulletProjectileMoveComponent>(TEXT("Projectile"));
	ProjectileMovement->bAutoActivate = false;
}

UImpactDataAsset* ABaseBulletProjectile::GetImpactDataAsset(const FHitResult& IHit)
{
	if (IHit.PhysMaterial.Get())
	{
		auto CurrentOwnerComp = Cast<ABaseWeaponItem>(WeaponOwner);
		
		const EPhysicalSurface LSurface = IHit.PhysMaterial->SurfaceType;

		if(CurrentOwnerComp)
		{
			UImpactDataAsset* Asset = CurrentOwnerComp->GetDataAsset()[EPhysicalSurface::SurfaceType_Default];

			if (CurrentOwnerComp->GetDataAsset().FindRef(LSurface))
			{
				Asset = CurrentOwnerComp->GetDataAsset()[LSurface];
			}

			return Asset;
		}
	}

	return nullptr;
}

void ABaseBulletProjectile::DeactivatePooling()
{
	SetPoolingActive(false);
}

void ABaseBulletProjectile::SetLifeSpan(float InLifespan)
{
	PoolingLifeSpan = InLifespan;

	GetWorldTimerManager().SetTimer(PoolingTimer, this, &ABaseBulletProjectile::DeactivatePooling, PoolingLifeSpan, false);
}

void ABaseBulletProjectile::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);

	GetWorldTimerManager().ClearTimer(PoolingTimer);
}

void ABaseBulletProjectile::SetPoolingActive(bool bInActive)
{
	check(AudioComponent);

	if (bInActive)
	{
		bPoolingActive = true;
		SetActorEnableCollision(true);
		SetActorHiddenInGame(false);
		BulletMesh->SetHiddenInGame(false);
		ProjectileMovement->Activate(true);
		ProjectileMovement->SetComponentTickEnabled(true);
		ProjectileMovement->RestartSimulation();
	}
	else if (bPoolingActive)
	{
		bPoolingActive = false;
		AudioComponent->Stop();
		ProjectileMovement->Deactivate();
		ProjectileMovement->SetComponentTickEnabled(false);
		SetActorHiddenInGame(true);
		AudioComponent->SetSound(nullptr);
	}
}

bool ABaseBulletProjectile::IsPoolingActive() const
{
	return bPoolingActive;
}

void ABaseBulletProjectile::InitSound(UObject* Sound, float Volume, float Pitch, bool bIs2D, USoundAttenuation* AttenuationOverride, USoundConcurrency* ConcurrencyOverride)
{
	if (!Sound) { return; }

	check(AudioComponent);

	if (USoundBase* SoundBase = Cast<USoundBase>(Sound))
	{
		AudioComponent->SetSound(SoundBase);
		AudioComponent->SetVolumeMultiplier(Volume);
		AudioComponent->SetPitchMultiplier(Pitch);
		if (bIs2D)
		{
			if (AudioComponent->bAllowSpatialization)
			{
				AudioComponent->bAllowSpatialization = false;
				AudioComponent->bIsUISound = true;
			}
		}
		else
		{
			if (!AudioComponent->bAllowSpatialization)
			{
				AudioComponent->bAllowSpatialization = true;
				AudioComponent->bIsUISound = false;
			}
			AudioComponent->AttenuationSettings = AttenuationOverride;
		}

		AudioComponent->ConcurrencySet.Empty();
		if (ConcurrencyOverride)
		{
			AudioComponent->ConcurrencySet.Add(ConcurrencyOverride);
		}
	}
}

void ABaseBulletProjectile::NotifyHit(UPrimitiveComponent* MyComp,
	AActor* Other,
	UPrimitiveComponent* OtherComp,
	bool bSelfMoved,
	FVector HitLocation,
	FVector HitNormal,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	if (bHideOnHit)
	{
		BulletMesh->SetHiddenInGame(true);
	}

	auto CurrentOwnerComp = Cast<ABaseWeaponItem>(WeaponOwner);
	
	if (Other && CurrentOwnerComp)
	{
		//Damage
		Other->TakeDamage(BulletData.BaseDamage, FDamageEvent(), nullptr, nullptr);
		
		if (CurrentOwnerComp->GetDataAsset().Num() > 0.f)
		{
			UImpactDataAsset* Assets = GetImpactDataAsset(Hit);
			
			if (Assets)
			{
				AudioComponent->SetSound(Assets->GetRandomSound());
				AudioComponent->Play();
			}
		}
	}

	SetActorEnableCollision(false);
}

void ABaseBulletProjectile::SetIgnoreActors(TArray<AActor*> Actors)
{
	Collider->MoveIgnoreActors = Actors;
}