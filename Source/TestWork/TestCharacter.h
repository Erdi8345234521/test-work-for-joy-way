// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Components/HealthComponent.h"
#include "Components/InventoryComponent.h"
#include "GameFramework/Character.h"
#include "TestCharacter.generated.h"

UCLASS()
class TESTWORK_API ATestCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	
	/** Sets default values for this character's properties */
	ATestCharacter();

	/** Character camera */
	UPROPERTY(VisibleAnywhere)
	UCameraComponent* Camera;

	/** Hand */
	UPROPERTY(VisibleAnywhere)
	USceneComponent* HandComponent;

	/** Character inventory */
	UPROPERTY(VisibleAnywhere)
	UInventoryComponent* InventoryComponent;

	/** Character health */
	UPROPERTY(VisibleAnywhere)
	class UHealthComponent* HealthComponent;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interact")
	TArray<TEnumAsByte<EObjectTypeQuery>> ObjectTypesInteract;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interact")
	float InteractDistance = 400.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Interact|Debug|Trace")
	bool bDebugTrace = true;

	/** Widget class for spawn inventory */
	UPROPERTY(EditAnywhere, category = "UI")
	TSubclassOf<UUserWidget> InventoryWidgetClass;
	
	UPROPERTY(BlueprintReadOnly, category = "UI")
	UUserWidget* InventoryWidget;

public:	
	
	/** Called to bind functionality to input */
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Handles moving forward/backward */
	void MoveForward(float Val);

	/** Handles stafing movement, left and right */
	void MoveRight(float Val);

	/** Handles on interact */
	void Interact();
	
	/** Shows and hides inventory */
	void ToggleInventory();

	/** Handles on pressed use item */
	void OnUseItem();
	
	/** Handles on released use item */
	void OnStopUseItem();

	/** Handles on click reload */
	void OnReload();
	
	/** Return character camera */
	FORCEINLINE UCameraComponent* GetCameraComponent() const { return Camera; }

	/** Return component hand, for items */
	FORCEINLINE USceneComponent* GetHandComponent() const { return HandComponent; }
};
